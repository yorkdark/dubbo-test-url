package com.darkstar.test;

import com.alibaba.dubbo.config.spring.ReferenceBean;
import com.darkstar.architecture.api.TestService;
import com.darkstar.architecture.api.dto.TestRequest;
import com.darkstar.architecture.api.dto.TestResponse;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by darkstar on 2018/1/10.
 */
public class BooTest {

    @Test
    public  void   druidmonitorlog(){

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("root.xml");
        String host="127.0.0.1:20881";
        String intername="com.darkstar.architecture.api.TestService";
        String url = "dubbo://"+host+"/"+intername;
        ReferenceBean<TestService> referenceBean = new ReferenceBean<TestService>();
        referenceBean.setApplicationContext(applicationContext);
        referenceBean.setInterface(TestService.class);
        referenceBean.setUrl(url);
        referenceBean.setVersion("1.0.0");
        try {
            referenceBean.afterPropertiesSet();
            TestService demoService = referenceBean.get();
            TestRequest request = new TestRequest();
            TestResponse jobRespanse= demoService.test(request);
            System.out.println(jobRespanse.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);



    }


}
